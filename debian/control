Source: cryptsetup-nuke-password
Section: admin
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Raphaël Hertzog <raphael@offensive-security.com>
Build-Depends: debhelper-compat (= 12), shunit2, po-debconf
Standards-Version: 4.3.0
Homepage: https://gitlab.com/kalilinux/packages/cryptsetup-nuke-password
Vcs-Browser: https://gitlab.com/kalilinux/packages/cryptsetup-nuke-password
Vcs-Git: https://gitlab.com/kalilinux/packages/cryptsetup-nuke-password.git

Package: cryptsetup-nuke-password
Architecture: any
Depends: cryptsetup-bin, ${shlibs:Depends}, ${misc:Depends}
Enhances: cryptsetup-initramfs
Description: Erase the LUKS keys with a special password on the unlock prompt
 Installing this package lets you configure a special "nuke password" that
 can be used to destroy the encryption keys required to unlock the encrypted
 partitions. This password can be entered in the usual early-boot prompt
 asking the passphrase to unlock the encrypted partition(s).
 .
 This provides a relatively stealth way to make your data unreadable in
 case you fear that your computer is going to be seized.
 .
 After installation, use “dpkg-reconfigure cryptsetup-nuke-password” to
 configure your nuke password.
